---
title: "Istruzioni per la creazione del mini-sito"
order: 0
in_menu: true
---
# Come creare questo mini-sito?

## 1° Crea il tuo mini-sito
- Segui l'indirizzo d'iscrizione [di Scribouilli](https://atelier.scribouilli.org/).
- Scegli **"il mio elenco di consiglio legati a Framalibre"** nell'elenco a discesa

## 2° Scegli i programmi da consigliare
- Naviga tra le schede di [Framalibre](https://beta.framalibre.org)
- Apri la scheda del programma che vuoi consigliare
- In **basso a sinistra** alla scheda, clicca su **"Copia per il mio mini-sito"**
- Un bout de code HTML vient de se copier dans votre presse-papier
- Il vous reste à **le coller dans la page de votre mini-site…**
- …et **recommencer** pour les suivants 
- et enfin, **publier la page** pour tout enregistrer !


![Le bouton se trouve tout en bas d'une notice](https://beta.framalibre.org/images/capture_bouton_copier_minisite.png) 