---
title: "La mia pagine dei suggerimenti"
order: 0
in_menu: true
---
C'est le moment de lister vos logiciels libres préférés, pour les recommander autour de vous !

Retrouvez les détails sur [https://beta.framalibre.org/mini-site](https://beta.framalibre.org/mini-site)

Un exemple ci-dessous :

# I programmi per tenersi informati

## Per tutti i miei filmati

<article class="framalibre-notice">
  <div>
    <img src="https://beta.framalibre.org/images/logo/PeerTube.png">
  </div>
  <div>
    <h2>PeerTube</h2>
    <p>PeerTube è una piattaforma decentralizzata e federata di condivisione di video.</p>
    <div>
      <a href="https://beta.framalibre.org/notices/peertube.html">Link a Framalibre</a>
      <a href="https://joinpeertube.org/fr/">Link al sito</a>
    </div>
  </div>
</article>

## Per ascoltare podcast

Nei miei momenti liberi mi piace ascoltare qualche podcast con:

  <article class="framalibre-notice">
    <div>
      <img src="https://beta.framalibre.org/images/logo/AntennaPod.png">
    </div>
    <div>
      <h2>AntennaPod</h2>
      <p>Ascolto di Podcast per Android.</p>
      <div>
        <a href="https://beta.framalibre.org/notices/antennapod.html">Link a Framalibre</a>
        <a href="http://antennapod.org/">Link al sito</a>
      </div>
    </div>
  </article> 